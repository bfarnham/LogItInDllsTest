Builds an executable and a shared library. General idea is that executable loads 
(with an explicit load_library() call) the shared lib at runtime. 
As the project name suggests, this is a test program to demonstrate and verify
logging with LogIt in a dynamic library

## windows ##
### build ###
I used the mingw prompt, but specify a visual studio project generation
```bash
# in an out-of-source build directory (here LogItInDllsTest-build-debug - a peer to the clone LogItInDllsTest)

# set BOOST environment variables (obviously depending on your boost)
export BOOST_PATH_HEADERS=/c/3rdPartySoftware/boost_mapped_namespace_builder/work/MAPPED_NAMESPACE_INSTALL/include/
export BOOST_PATH_LIBS=/c/3rdPartySoftware/boost_mapped_namespace_builder/work/MAPPED_NAMESPACE_INSTALL/lib/

# generate: Note explicit visual studio specific generator
cmake ../LogItInDllsTest -DCMAKE_TOOLCHAIN_FILE=custom_boost_win_VS2017.cmake -DCMAKE_BUILD_TYPE=Debug -G "Visual Studio 15 2017 Win64"

# build: Note cmake delegates building to visual studio
cmake --build $(pwd) --config Debug
```
Note for visual studio users, this will create a handy LogItInDllsTest.sln, double click to open project in IDE.

### run ###
```bat
:: from my build directory D:\workspace\OPC-UA\LogItInDllsTest-build-debug\theEXE\Debug
:: set windows PATH variable to locate theDYNLIB.dll
PATH=%PATH%;D:\workspace\OPC-UA\LogItInDllsTest-build-debug\theDYNLIB\Debug

:: then run theEXE.exe
PATH=%PATH%;D:\workspace\OPC-UA\LogItInDllsTest-build-debug\theDYNLIB\Debug
theEXE.exe
```

## linux ##
### build ###
```bash
# set BOOST environment variables (again, depends on your boost)
export BOOST_PATH_HEADERS=/local/bfarnham/workspace/boost_mapped_namespace_builder/work_1_68_0/MAPPED_NAMESPACE_INSTALL/include/
export BOOST_PATH_LIBS=/local/bfarnham/workspace/boost_mapped_namespace_builder/work_1_68_0/MAPPED_NAMESPACE_INSTALL/lib/
export BOOST_LIB_SUFFIX=-gcc48-mt-x64-1_68

# generate (just regular Unix makefiles, no need to state explicit generator)
cmake ../LogItInDllsTest -DCMAKE_TOOLCHAIN_FILE=custom_boost_cc7.cmake -DCMAKE_BUILD_TYPE=Debug 

# build
cmake --build $(pwd) --config Debug
```

### run ###
```bash
# from my build directory /local/bfarnham/workspace/OPC-UA/LogItInDllsTest-build-debug/theEXE
# set LD_LIBRARY_PATH to locate theDYNLIB.so
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/local/bfarnham/workspace/OPC-UA/LogItInDllsTest-build-debug/theDYNLIB/

# then run theEXE
./theEXE
```
