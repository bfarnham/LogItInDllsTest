#include "DynLibMain.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <LogIt.h>
#include <LogLevels.h>

std::string DynLib::getGreeting(void)
{
	return "hello from DYNLIB";
}

bool DynLib::initialiseLogging(LogItInstance* remoteInstance, const size_t& numOfComponents)
{
	std::cout << __FUNCTION__ << " initialise logging called with ["<<std::hex<<remoteInstance<<"] numOfComponents ["<<numOfComponents<<"]" << std::endl;
	Log::initializeDllLogging(remoteInstance);
	for (size_t i = 0; i < numOfComponents; ++i)
	{
		std::ostringstream name;
		name << "DLL_component_" << i;
		Log::registerLoggingComponent(name.str(), Log::INF);
	}
	LOG(Log::INF) << __FUNCTION__ << " logging initialised - first log message from the DLL, current log handle count [" << Log::getComponentLogsList().size() << "] components";
	return false;
}

void DynLib::doSomeWork(const size_t i)
{
	LOG(Log::INF) << __FUNCTION__ << "+ i [" << i << "]";
	LOG(Log::INF) << __FUNCTION__ << "- i [" << i << "]";
}
