#pragma once

/**
* If building a dll, windows needs to know which functions are externally visible
*/
#ifdef _WINDOWS
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT 
#endif //_WINDOWS
