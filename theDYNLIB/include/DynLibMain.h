#pragma once

#include <string>
#include "ExportMacroDefinition.h"
#include <LogIt.h>

namespace DynLib
{
	DLL_EXPORT std::string getGreeting(void);
	DLL_EXPORT bool initialiseLogging(LogItInstance* remoteInstance, const size_t& numOfComponents);
	DLL_EXPORT void doSomeWork(const size_t i);
}