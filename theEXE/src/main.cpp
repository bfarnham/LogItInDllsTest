#include <iostream>
#include <thread>
#include <chrono>
#include <LogIt.h>
#include <LogLevels.h>
#include <stdexcept>
#include <cstdlib>

#include "DynLibMain.h"

void initialiseExeLogging(const size_t numOfComponents)
{
	Log::initializeLogging(Log::TRC);
	for (size_t i = 0; i < numOfComponents; ++i)
	{
		std::ostringstream name;
		name << "EXE_component_" << i;
		Log::registerLoggingComponent(name.str(), Log::INF);
	}
	LOG(Log::INF) << __FUNCTION__ << "- target ["<<numOfComponents<<"], current log handle count ["<<Log::getComponentLogsList().size()<<"] components";
}

void doSomeWork(const size_t& i)
{
	LOG(Log::INF) << __FUNCTION__ << "+ i [" << i << "]";
	LOG(Log::INF) << __FUNCTION__ << "- i [" << i << "]";
}

void exeThreadFn(const size_t& numOfIterations)
{
	const auto startPoint = std::chrono::high_resolution_clock::now();
	LOG(Log::INF) << __FUNCTION__ << "+";
	for (size_t i = 0; i < numOfIterations; ++i)
	{
		/*
		const int randomSleepMs = (std::rand() % 3) + 1; // between 1 and 3
		LOG(Log::INF) << __FUNCTION__ << " logging [" << i << "], sleep [" << randomSleepMs << "ms]";
		std::this_thread::sleep_for(std::chrono::milliseconds(randomSleepMs));
		*/
		doSomeWork(i);
	}
	const auto endPoint = std::chrono::high_resolution_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(endPoint - startPoint).count();
	LOG(Log::INF) << __FUNCTION__ << "- duration [" << duration << "usec] average [" << duration / float(numOfIterations) << "]";
}

void dllThreadFn(const size_t& numOfIterations)
{
	const auto startPoint = std::chrono::high_resolution_clock::now();
	LOG(Log::INF) << __FUNCTION__ << "+";
	for (size_t i = 0; i < numOfIterations; ++i)
	{
		/*
		const int randomSleepMs = (std::rand() % 3) + 1; // between 1 and 3
		LOG(Log::INF) << __FUNCTION__ << " logging [" << i << "], sleep [" << randomSleepMs << "ms]";
		std::this_thread::sleep_for(std::chrono::milliseconds(randomSleepMs));
		*/
		DynLib::doSomeWork(i);
	}
	const auto endPoint = std::chrono::high_resolution_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(endPoint - startPoint).count();
	LOG(Log::INF) << __FUNCTION__ << "- duration [" << duration << "usec] average ["<<duration/float(numOfIterations)<<"]";
}

int main(int argc, char * argv[])
{
	std::cout << __FUNCTION__ << "+ Initialising global Log::LogInstance in theEXE" << std::endl;
	initialiseExeLogging(100);

	std::cout << __FUNCTION__ << " Initialising Log::LogInstance in theDLL" << std::endl;
	DynLib::initialiseLogging(LogItInstance::getInstance(), 100);

	LOG(Log::INF) << __FUNCTION__ << " check DLL loaded, greeting from DLL is [" << DynLib::getGreeting() << "]";
	
	LOG(Log::INF) << __FUNCTION__ << " starting logging threads in theEXE and theDLL";
	std::thread exeThread(exeThreadFn, 5000);
	std::thread dllThread(dllThreadFn, 5000);
	exeThread.join();
	dllThread.join();
	
	std::cout << __FUNCTION__ << "-" << std::endl;
}
